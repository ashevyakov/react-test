'use strict'

import AbstractDowloader from './abstract-downloader'
import fileManager from '../_db-manager'
import downloader from '../content-downloader'

const axios = require('axios')
const querystring = require('querystring')
const parser = require('node-html-parser')

module.exports = class ReadComicOnlineDownloader extends AbstractDowloader {

	constructor (url) {
		super(url)

		let urlFolders = url.replace(/\/{2}/g, '/').
			replace(/(\/?)$/, '').
			split(/\//)

		if (urlFolders.length) {
			this.name = urlFolders[urlFolders.length - 1]
			fileManager.setEnv(this.name)
		}

	}

	/**
	 * @param {string} url
	 * */
	static download (url) {
		url = 'https://readcomiconline.to/Comic/Edge-of-Spider-Verse'
		new ReadComicOnlineDownloader(url).run()
		// new UniDownloader(url);
		// dbManager.generateSummary();
	}

	static parseLoadingPage (data, url) {

		const elementValue = (doc, name) => {
			try {
				return doc.querySelector(`[name=${name}]`).attributes.value
			} catch (e) {
				return ''
			}
		}

		const getCode = (data) => {
			let match = data.match(
				/setTimeout\(function\(\){([\s\S]*?)}, \d{1,5}\);/)
			let magicNumber = null
			if (match && match.length > 1) {
				let script = match[1]
				script = script.replace(/\w{1,3}\.action \+= location\.hash;/, '')
				script = script.replace(/\w{1,3}\.submit\(\);/, '')
				script = script.replace(
					`t = t.firstChild.href;r = t.match(/https?:\\/\\//)[0];`,
					't = "' + url + '";')
				script = script.replace(
					't = t.substr(r.length); t = t.substr(0,t.length-1);', '')

				let code = `var document = {getElementById: function(){return {value: ''}},
                    createElement: function(){return {innerHTML: '', firstChild: {href: '${url}'}}}};
                ${script}
                return a.value;`
				magicNumber = new Function(code)()
			}

			return magicNumber
		}

		let page = parser.parse(data)
		let attrs = {
			's': elementValue(page, 's'),
			'jschl_vc': elementValue(page, 'jschl_vc'),
			'pass': elementValue(page, 'pass'),
		}

		let magicNumber = getCode(data)
		if (magicNumber === null) {
			console.error('Could not get a secret number')
			return
		}

		attrs['jschl_answer'] = magicNumber

		return attrs
	}

	run () {

		axios.defaults.withCredentials = true

		const instance = axios.create({
			baseURL: this.url,
			withCredentials: true,
			timeout: 10000,
			headers: {},
		})

		instance.get(this.url, { withCredentials: true }).then().catch(error => {

			let data = error.response.data

			let page = parser.parse(data)
			let attrs = ReadComicOnlineDownloader.parseLoadingPage(data, this.domain)

			let action = ''

			try {
				action = page.querySelector('#challenge-form').attributes.action
			} catch (e) {
				console.error('Count not get an action url')
				return
			}
			// __cfduid=d6ad706e47661a429305cac6d8ce7ec811549516550; expires=Fri, 07-Feb-20 05:15:50 GMT; path=/; domain=.readcomiconline.to; HttpOnly; Secure
			let cookie = error.response.headers['set-cookie'][0].split(';')[0]
			setTimeout(() => {

				let query = querystring.stringify(attrs)

				instance.get(action, {
					params: attrs, withCredentials: true, headers: {
						'connection': 'keep-alive',
						'upgrade-insecure-requests': '1',
						'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36',
						'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
						'accept-encoding': 'gzip, deflate, br',
						'accept-language': 'en-GB,en-US;q=0.9,en;q=0.8',
						'cookie': cookie,
						'referer': this.url,
						':authority': 'readcomiconline.to',
						':method': 'GET',
						':path': '/cdn-cgi/l/chk_jschl?' + query,
						':scheme': 'https',
					},
				}).then((response) => {
					console.error('YES!', response.statusCode)
				}).catch(error => {
					console.error('Nope', error)
					let a = Object.keys(error.request).map(key => {
						return key
					})
					console.error(a)
					console.error(error.request._header)

				})
			}, 4000)

		})

//         downloader.add(this.url, (data, response) => {
//
//             let page = parser.parse(data);
//             let attrs = ReadComicOnlineDownloader.parseLoadingPage(data, this.domain);
//
//             let action = '';
//
//             try {
//                 action = page.querySelector('#challenge-form').attributes.action;
//             } catch (e) {
//                 console.error('Count not get an action url');
//                 return;
//             }
//
//             action = `${this.domain}${action}`;
// // console.error(this);
//             setTimeout(() => {
//                 downloader.add(action, function(data) {
//                     console.error('?');
//                 }, {
//                     params: attrs,
//                     headers : {
//                         // 'Host': this.host,

//                     }
//                 });
//             }, 4000);
//
//
//         }, {
//             statusCode: 503,
//             headers : {
//             // 'Host': this.host,
//             'Connection': 'keep-alive',
//                 'Upgrade-Insecure-Requests': '1',
//                 'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36',
//                 'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
//                 'Accept-Encoding': 'gzip, deflate, br',
//                 'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
//                 'Referer': this.url
//             }
//         });
	}

	_onImageDownloadComplete (dataStream) {
		let filename = this.replace(/.*?\/([\w\.]*)$/, '$1')
		let chapter = this.replace(/.*?(\d*?)\/[\w\.]*$/, '$1')
		fileManager.writeStreamIntoFile(filename, chapter, dataStream)
	}

	_downloadNext (body) {

		let page = parser.parse(body),
			links = page.querySelectorAll('.block a'),
			image = page.querySelector('#image'),
			nextPage = null


		links.forEach(link => {
			let imageLink = link.querySelector('#image')
			if (imageLink) {
				nextPage = link.attributes.href
			}
		})

		if (nextPage == null) {
			console.error('Could not identify the next page')
			return
		}

		downloader.add(this.domain + nextPage, this._downloadNext.bind(this))
		downloader.add(image.attributes.src,
			this._onImageDownloadComplete.bind(image.attributes.src))
	}

	_onTitlePageDownloadComplete (titlePage) {

		let parsedTitlePage = parser.parse(titlePage)
		let titleLink = parsedTitlePage.querySelector('.bigChar')
		console.error('LINK', titleLink)

		// let title = parsedTitlePage.querySelector('h1').text;
		// dbManager.saveSummary({title: title, name: this.name});
		//
		// if(button && button.attributes) {
		//
		//     let linkToFirstPage = button.attributes.href;
		//     let onComplete = function() {
		//         dbManager.generateSummary();
		//     }.bind(this);
		//
		//     downloader.add(linkToFirstPage, this._downloadNext.bind(this));
		//     downloader.onComplete(onComplete);
		// }

	}

}