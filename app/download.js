'use strict'

import DownloadManager from './comic-downloaders/download-manager'
import {url} from './util/params'

DownloadManager.download(url)