import Page from './Page'
import PropTypes from 'prop-types'

class Chapter {

	constructor (id) {
		this._id = id
	}

	get id() {
		return this._id
	}

	set id(id) {
		this._id = id
	}

	toJSON() {
		return {}
	}

	static create(id) {
		return new Chapter(id)
	}

}

Chapter.propTypes = {
	id: PropTypes.number.isRequired,
	path: PropTypes.string.isRequired,
	description: PropTypes.string
}

export default Chapter