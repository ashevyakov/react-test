import Comicbook from './Comicbook'

class Database {

	constructor() {
		this._comicbooks = {}
	}

	/** @param {Comicbook} comicbook */
	addComicbook(comicbook) {
		this._comicbooks[comicbook.name] = comicbook
	}

	findComicbook(name) {
		return this._comicbooks[name]
	}

	getComicbookJSONList() {
		let result = []
		Object.keys(this._comicbooks).map(key =>
			result.push(this._comicbooks[key].toJSON(true))
		)

		return result
	}

	getComicbooksByQuery(query, limit) {
		if (query) {
			let count = 0
			let list = Object.values(this._comicbooks).filter(comicbook => {
				if(count >= limit) return

				let result = true
				Object.keys(query).map(key => {
					result = result && comicbook[key] === query[key]
				})
				count += result ? 1: 0
				return result
			})

			return list.map(comicbook => comicbook.toJSON(true))
		}

		return []
	}

	getComicbookByQuery(query) {
		let result = this.getComicbooksByQuery(query, 1)
		return result.length === 1 ? result[0]: {}
	}

	getChapters(query) {
		let comicbook = this.findComicbook(query.comicbookName)
		if(comicbook) {
			let chapters = comicbook.getChapters()
			return chapters.map(chapter => {
				let title = `${comicbook.title} #${chapter}`
				return {id: chapter, title: title}
			})
		}

		return []
	}

	getPages(query) {
		let comicbook = this.findComicbook(query.comicbookName)
		if(comicbook) {
			return comicbook.getPages(query)
		}

		return []
	}

	/** @return {Database} */
	fromJSON(data) {

		if (data && data.comicbooks) {
			Object.keys(data.comicbooks).map(key => {
				this._comicbooks[key] = Comicbook.create(data.comicbooks[key])
			})
		}

		return this
	}

	/** @return {object} */
	toJSON() {
		let data = {}
		data.comicbooks = {}

		Object.keys(this._comicbooks).map(key => {
			data.comicbooks[key] = this._comicbooks[key].toJSON()
		})

		return data
	}

	/** @return {Database} */
	static create(data) {
		return new Database().fromJSON(data)
	}

}

export default Database