class Page {

	fromJSON({ key, path, chapter }) {
		this._key = key
		this._path = path
		this._chapter = chapter

		return this
	}

	get key() {
		return this._key
	}

	set key(value) {
		this._key = value
	}

	get path() {
		return this._path
	}

	set path(value) {
		this._path = value
	}

	get chapter() {
		return this._chapter
	}

	set chapter(value) {
		this._chapter = value
	}

	toJSON () {
		return {
			key: this._key,
			path: this._path,
			chapter: this._chapter
		}
	}

	static create(data) {
		return new Page().fromJSON(data)
	}

}

export default Page