import Page from './Page'

class Comicbook {

	fromJSON({ name, title, preview, description, pages, chaptersCount }) {
		this._name = name
		this._title = title ? title: ''
		this._description = description ? description: ''
		this._preview = preview ? preview: ''
		this._pages = []

		if(pages) {
			pages.map(page => {
				this.addPage(page)
			})
		}

		this._chaptersCount = chaptersCount ? chaptersCount: 0

		return this
	}

	toJSON (nopages) {

		let data = {
			name: this._name,
			description: this._description,
			title: this._title,
			preview: this._preview,
			chaptersCount: this._chaptersCount
		}

		if(!nopages) {
			data.pages = []

			this._pages.map(item => {
				data.pages.push(item.toJSON())
			})
		}

		return data
	}

	get title() {
		return this._title
	}

	set title (value) {
		this._title = value
	}

	set preview (value) {
		this._preview = value
	}

	get description() {
		return this._description
	}

	get preview () {
		return this._preview
	}

	get name () {
		return this._name
	}

	getChapters() {
		return this._pages.map(item => item.chapter).filter((e, i, arr) => arr.indexOf(e) === i)
	}

	/** @return {Page} */
	addPage(data) {
		let page = this.findPage(data.key)
		if(page) return page

		page = Page.create(data)
		this._pages.push(page)

		data.chaptersCount = this.getChapters().length

		return page
	}

	findPage(key) {
		return this._pages.find(page => key === page.key)
	}

	getPages(query) {
		let pages = []

		this._pages.map(item => {
			pages.push(item.toJSON())
		})

		return pages
	}

	static create(data) {
		return new Comicbook().fromJSON(data)
	}

}

export default Comicbook