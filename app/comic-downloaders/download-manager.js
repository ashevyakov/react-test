'use strict'

const UniDownloader = require('../comic-downloaders/uni-downloader')

module.exports = class Manager {

	/**
	 * @param {string} url
	 * */
	static download (url) {
		if (url.match(/^http:\/\/unicomics.ru/)) {
			UniDownloader.download(url)
		} else {
			console.error('Could not find a downloader')
		}
	}

}