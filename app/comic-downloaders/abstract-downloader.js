'use strict'

import { extractDetailsFromURL } from '../util/content-downloader'

export default class DowloaderInterface {

	constructor (url) {

		let { protocol, host } = extractDetailsFromURL(url)

		this.domain = `${protocol}://${host}`
		this.url = url

	}

	run () {}

}

