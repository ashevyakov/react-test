'use strict'

import AbstractDowloader from './abstract-downloader'
import dbManager from '../util/db-manager'
import downloader from '../util/content-downloader'

const parser = require('node-html-parser')


module.exports = class UniDownloader extends AbstractDowloader {

	constructor (url) {
		super(url)

		let urlFolders = url.replace(/\/{2}/g, '/').
			replace(/(\/?)$/, '').
			split(/\//)
		if (urlFolders.length) {
			this.name = urlFolders[urlFolders.length - 1]

			/** @type {Comicbook} this.comicbook */
			this.comicbook = dbManager.createComicbook({name: this.name})
		}

	}

	/**
	 * @param {string} url
	 * */
	static download (url) {
		new UniDownloader(url).run()
		// new UniDownloader(url);
		// dbManager.generateSummary();
	}

	run () {
		downloader.add(this.url, this._onTitlePageDownloadComplete.bind(this))
	}

	_onImageDownloadComplete (comicbook, dataStream) {
		dbManager.addComicbookImage(comicbook, this, dataStream)
	}

	_downloadNext (body) {

		let page = parser.parse(body),
			links = page.querySelectorAll('.block a'),
			image = page.querySelector('.image_online'),
			nextPage = null

		let _page
		let chapterName = page.querySelector('h2').text
		if(chapterName) {
			let split = chapterName.split('#')
			if(split.length > 1) {
				let filename = image.attributes.src.replace(/.*?\/([\w\.]*)$/, '$1')
				_page = dbManager.addPage(this.comicbook, split[1], filename)
			} else {
				console.error('Cannot define chapter number. Exiting')
				return
			}
		}

		links.forEach(link => {
			let imageLink = link.querySelector('.image_online')
			if (imageLink) {
				nextPage = link.attributes.href
			}
		})

		if (nextPage == null) {
			console.error('Could not identify the next page')
			return
		}

		downloader.add(this.domain + nextPage, this._downloadNext.bind(this))
		if(_page) {
			downloader.add(image.attributes.src, this._onImageDownloadComplete.bind(_page, this.comicbook))
		}
	}

	_onTitlePageDownloadComplete (titlePage) {

		downloader.setTimer(10);

		let parsedTitlePage = parser.parse(titlePage)
		let button = parsedTitlePage.querySelector('.button.read-online-series a')

		let previewTag = parsedTitlePage.querySelector('div.image.center img')
		if(previewTag && !this.comicbook.preview) {
			downloader.add(previewTag.attributes.src, (dataStream, extension) => {
				dbManager.addPreview(this.comicbook, dataStream, extension)
			})
		}

		this.comicbook.title = parsedTitlePage.querySelector('h1').text

		if (button && button.attributes) {

			let linkToFirstPage = button.attributes.href
			let onComplete = function () {
				dbManager.commit()
			}.bind(this)

			downloader.add(linkToFirstPage, this._downloadNext.bind(this))
			downloader.onComplete(onComplete)
		}

	}

}