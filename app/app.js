import path from 'path'
import express from 'express'
import graphqlHTTP from 'express-graphql'
import { buildSchema } from 'graphql'
import cors from 'cors'
import dbManager from './util/db-manager'

let app = express()

let schema = buildSchema(`

  type Query {
      comics: [Comicbook!]!
      comic(name: ID!): Comicbook
      chapters(comicbookName: String): [Chapter]
      pages(comicbookName: String): [Page]
  },

  type Comicbook {
		name: ID!
    title: String
    description: String
    preview: String
    chaptersCount: Int
  }
  
  type Chapter {
    id: ID!
    title: String
  }
  
  type Page {
    key: ID!
    path: String!
    chapter: String!
  }

`)

let root = {
	comics: () => dbManager.getComicbookList(),
	comic: query => dbManager.getComicbook(query),
	chapters: query => dbManager.getChapters(query),
	pages: query => dbManager.getPages(query)

}

app.use('/', express.static(path.join(__dirname, 'public')))
app.use(cors())

// let root = {
// 	comics: () => comics,
// 	comic: query => comics.find(comic => {
// 		console.error(typeof query.id, typeof comic.id)
// 		return query.id === comic.id
// 	}),
// }

app.use('/comics', graphqlHTTP({
	schema: schema,
	rootValue: root,
	graphiql: true,
}))

app.listen(8000, () => console.log('Now browse to localhost:8000/comics'))

// {
// 	comic(name: "captain-america-hail-hydra") {
// 	name
// 	title
// 	description
// 	preview
// 	chaptersCount
// }
// }


// router.route('/create/:name').get((req, res) => {
//
//     res = wrapResponce(res);
//     let { name } = req.params;
//     dbManager.setEnv(name);
//     dbManager.generateSummary();
//
//     res.json({result: true});
// });
//
// router.route('/list').get((req, res) => {
//     res = wrapResponce(res);
//     let list = dbManager.getComicList();
//     res.json(list);
// });
//
// router.route('/list/:name').get((req, res) => {
//     res = wrapResponce(res);
//
//     let { name } = req.params;
//     let comicData = dbManager.getComicChapterByName(name);
//     if(comicData == null) {
//         res.status(404).send('Not found');
//         return;
//     }
//     res.json(comicData);
// });
//
// router.route('/details/:name').get((req, res) => {
//     res = wrapResponce(res);
//
//     let { name } = req.params;
//     let pageDetails = dbManager.getPages(name);
//     if(pageDetails == null) {
//         res.status(404).send('Not found');
//         return;
//     }
//     res.json(pageDetails);
// });
// router.route('/img/:name/:chapter/:page').get((req, res) => {
//
//     let { name, chapter, page } = req.params;
//     let img = dbManager.getImage(name, chapter, page);
// console.error(img);
//     if(!img) {
//         res.status(404).send('Not found');
//         return;
//     }
//
//     let mimeType = mime.lookup(img);
//     let stream = fs.createReadStream(img);
//
//     stream.on('open', function () {
//         res.set('Content-Type', mimeType);
//         stream.pipe(res);
//     });
//
//     stream.on('error', function () {
//         res.set('Content-Type', 'text/plain');
//         res.status(404).end('Not found');
//     });
// });

//
// dbManager.addComicbook({
// 	'title': 'За Локи',
// 	'name': 'vote-loki',
// 	'description': 'No description',
// 	'preview': ''
// })
//
// dbManager.commit()
//
// let comics = [
// 	Comicbook.create({ id: '1', title: 'Brian', name: '21', preview: 'preview' }),
// 	Comicbook.create({ id: '2', title: 'Chris', name: '22', preview: 'preview2' }),
// ]
//
