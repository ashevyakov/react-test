'use strict'

import * as fs from 'fs'
import * as path from 'path'
import {downloadDir} from '../util/params'
import Comicbook from '../entities/Comicbook'
import Database from '../entities/Database'
import Page from '../entities/Page'

/** @typedef {Object} process */
/** @typedef {Function} fs.writeFileSync */
/** @typedef {Function} fs.mkdirSync */
/** @typedef {Function} fs.readFileSync */
/** @typedef {Function} fs.existsSync */

class DbManager {

	constructor () {
		this._downloadDir = downloadDir
		this._dbFile = path.join(this._downloadDir, 'db.json')

		fs.mkdirSync(this._downloadDir, { recursive: true })
		let data = this._load()
		if(!data) {
			data = {}
		}

		/** @typedef {Database} this.db */
		this._db = Database.create(data)
	}

	createComicbook(data) {
		if(!data.name) return false

		let comicbook = this._db.findComicbook(data.name)
		if(!comicbook) {
			comicbook = Comicbook.create(data)
			this._db.addComicbook(comicbook)
		}

		return comicbook
	}

	/**
	 * @param {Comicbook} comicbook
	 * @param {object} dataStream
	 * @param {string} extension
	 * */
	addPreview(comicbook, dataStream, extension) {
		if(!comicbook) return false

		comicbook.preview = `preview.${extension}`
		let filename = path.join(this._downloadDir, comicbook.name, comicbook.preview)
		this._writeStreamIntoFile(filename, dataStream)
	}

	addComicbookImage(comicbook, page, dataStream) {
		if(!comicbook || !page) return false

		let chapterDir = path.join(this._downloadDir, comicbook.name)
		fs.mkdirSync(chapterDir, { recursive: true })

		let filename = path.join(chapterDir, page.path)
		this._writeStreamIntoFile(filename, dataStream)
		this.commit()
	}

	/**
	 * @param {Comicbook} comicbook
	 * @param {string} chapterNumber
	 * @param {string} filename
	 *
	 * @return {Page}
	 */
	addPage(comicbook, chapterNumber, filename) {
		let _path = path.join(chapterNumber, filename)
		let page = comicbook.addPage({key: _path, chapter: chapterNumber, path: _path})

		return this._verifyComicbookImage(comicbook, page.path) ? page: false
	}

	getComicbookList() {
		return this._db.getComicbookJSONList()
	}

	getComicbook(query) {
		return this._db.getComicbookByQuery(query)
	}

	getChapters(query) {
		if(query && query.comicbookName) {
			return this._db.getChapters(query)
		}

		return []
	}

	/**
	 * @param {object} query
	 * @param {object} query.comicbookName
	 * */
	getPages(query) {
		if(query && query.comicbookName) {
			return this._db.getPages(query)
		}

		return []
	}

	commit() {
		fs.writeFileSync(this._dbFile, JSON.stringify(this._db.toJSON()), { flag: 'w' })
	}

	/** @private */
	_verifyComicbookImage(comicbook, pagePath) {
		if(!comicbook || !pagePath) return false

		let filename = path.join(this._downloadDir, comicbook.name, pagePath)
		return !fs.existsSync(filename)
	}
	/** @private */
	_load() {

		let result = false
		try {
			let content = fs.readFileSync(this._dbFile, 'utf8')
			if(content) result = JSON.parse(content)
		} catch (e) {
			console.warn('Couldn\'t load db file')
			return false
		}

		return result
	}
	/** @private */
	_writeStreamIntoFile (filename, dataStream) {
		fs.mkdirSync(path.dirname(filename), { recursive: true })
		fs.writeFileSync(filename, dataStream.read())
	}

}

let dbManager = new DbManager()

module.exports = dbManager