/** @typedef {object} process */
/** @typedef {object} */
let args = process.argv, downloadDir = '', _url = ''
let length = args.length

process.argv.map((arg, index) => {
	if(arg === '-d' && length > index + 1) downloadDir = args[index + 1]
	if(arg === '-u' && length > index + 1) _url = args[index + 1]
})


if(!downloadDir) {
	console.error('Download dir is not pointed. Use key -u {url}')
	process.exit()
}

export { downloadDir }

export const url = (function() {
	if(!_url) {
		console.warn('Url is not found. Use key -u {url}')
	}

	return _url
})()