'use strict'

const http = require('http')
// const util      = require('./file-manager');
const Stream = require('stream').Transform

export function extractDetailsFromURL (url) {
	let path = url != null ? url + '' : ''
	let matches = path.match(/(http[s]?):\/\/?([\S\s]*?)(\/[\S\s]*)/i)

	let protocol = 'http', port = 80, host = '', uri = ''

	if (matches) {
		if (matches[1]) {
			protocol = matches[1]
			port = protocol === 'https' ? 443 : port
		}
		if (matches[2]) host = matches[2]
		if (matches[3]) uri = matches[3]
	}

	return { host: host, port: port, path: uri, protocol: protocol }
}

class ContentDownloader {

	constructor () {

		this.limit = 1
		this.queue = []
		this.busy = false

		this.cache = {}
		this.idle = 0
		this.interval = setInterval(this.downloadNext.bind(this), 500 + 3000 * Math.random())
		this.onCompleteCallback = function () {}
	}

	onComplete (callback) {
		this.onCompleteCallback = callback
	}

	setTimer(time) {
		// setTimeout(() => {
		// 	this.idle = 150
		// 	this.downloadNext()
		// },time * 1000)
	}

	downloadNext () {

		this.idle++
		if (this.idle > 120) {
			clearInterval(this.interval)
			if ('function' === typeof this.onCompleteCallback) {
				this.onCompleteCallback()
			}
		}

		if (this.busy) return
		if (!this.queue.length) return

		this.idle = 0

		for (let i = 0; i < this.limit && i < this.queue.length; i++) {
			let next = this.queue.shift()

			let { host, port, path } = extractDetailsFromURL(next.url)
			console.log('download: ', next.url)
			http.get({ host: host, port: port, path: path }, function (next, res) {
				if (res.statusCode >= 200 && res.statusCode <= 302) {
					let contentType = res.headers['content-type']
					if (contentType === 'image/jpeg' || contentType === 'image/png' ||
						contentType === 'image/gif') {
						let data = new Stream()
						let extension = contentType.indexOf('gif') > -1 ? 'git': (contentType.indexOf('png') > -1 ? 'png': 'jpg')

						res.on('data', function (chunk) {
							data.push(chunk)
						})

						res.on('end', function () {
							if (next.callback) next.callback(data, extension)
						})

					} else {

						let body = ''

						res.on('data', function (chunk) {
							body += chunk
						})

						res.on('end', function () {
							if (next.callback) next.callback(body)
						})

					}

				}

			}.bind(this, next)).on('error', function (e) {
				console.log('Got error: ' + e.message)
			})

		}

	}

	add (url, callback) {
		if (this.cache[url]) return
		this.cache[url] = true
		this.queue.push({ url: url, callback: callback })
	}

}

export default new ContentDownloader()
